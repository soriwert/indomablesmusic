<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\User;

class VideoController extends Controller
{

    public function index()
    {
        //$this->authorize('view', Video::class);
        $videos = Video::paginate(10);
        $videos = Video::all();
        return view('videos.index', ['videos' => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Video::class);
        return view('videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update', Video::class);

        //validacion trait
        $this->validate($request, [
            'url' => 'required|max:255',
            'titulo' => 'required|max:50',
            'artista' => 'required|max:50',
            'ciudad' => 'required',
            'year' => 'required'
        ]);
        $video = new Video($request->all());
        $video->save();
        return redirect('/videos');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Video::class);
        $video = Video::where('id', $id)->first();
        return view('videos.edit', ['video' => $video]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Video::class);
        $this->validate($request, [
            'url' => 'required|max:255',
            'titulo' => 'required|max:50',
            'artista' => 'required|max:50',
            'ciudad' => 'required',
            'year' => 'required'
        ]);
        $video = Video::find($id);
        $video->fill($request->all());
        $video->save();
        return redirect('/videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Video::class);
        Video::destroy($id);
        return redirect("/videos");
    }
}
