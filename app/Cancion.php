<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancion extends Model
{
    public $table = "canciones";
    public $timestamps = false;
protected $fillable = ['id', 'url', 'titulo', 'artista', 'ciudad', 'year', 'urlImg'];
}
