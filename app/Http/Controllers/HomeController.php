<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }
    public function login()
    {
        return view('home');
    }
    public function logout()
    {
        return view('home');
    }



    public function eventos()
    {
        return view('eventos');
    }

    public function merchandising()
    {
        return view('merchandising');
    }

}
