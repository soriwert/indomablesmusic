/*
window.onload = function () {

    var ocultaLogin = document.getElementById("iconLog");
    console.log(ocultaLogin);
    ocultaLogin.addEventListener("click", function() {
        console.log("saludo");
    });
}

*/
window.onload = function () {
  document.addEventListener("scroll", mantenerMenu);
  document.getElementById("iconLog").addEventListener("click", mostrarLogin);



  tinymce.init({
    selector: 'textarea',
    height: 200,
    menubar: false,
    plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
    ],
    toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
  });

};



function mantenerMenu() {
  if ($(document).scrollTop() >= 527) {
    document.getElementById("nav").style.position = "fixed";
  } else {
    document.getElementById("nav").style.position = "relative";
  }
}

function mostrarLogin() {
  var iconLog = document.getElementById("iconLog");
  var iconClassLog = document.getElementsByClassName("iconClassLog");
  var formLog = document.getElementById("login");
  iconLog.style.display = "none";
  formLog.style.display = "inherit";
  formLog.addEventListener("mouseover", function () {
    iconLog.style.display = "none";
    formLog.style.display = "inherit";
  })
  formLog.addEventListener("mouseout", function () {
    iconLog.style.display = "inherit";
    formLog.style.display = "none";
  })
}

function volume() {
  var volumen = document.getElementById("volume");
  volumen.addEventListener("change", function(){
    return volumen.value;
  });
}

var colorAntiguo = "";
function cambiaCancion(url, id, titulo, artista, ciudad, year, rutaImg) {
  if(colorAntiguo != ""){
    colorAntiguo.style.color = "RGB(0,123,255)";
  }
  var audio = document.getElementsByTagName('audio');
  audio[0].src = url;
  audio[0].play();
  document.getElementById(titulo+id).style.color = "orange";
  document.getElementById("play").addEventListener("click", function () {
    audio[0].play();
    document.getElementById(titulo+id).style.color = "orange";
  });
  document.getElementById("pause").addEventListener("click", function () {
    audio[0].pause();
  });
  document.getElementById("stop").addEventListener("click", function () {
    audio[0].load();
    document.getElementById(titulo+id).style.color = "RGB(0,123,255)";
  });
  var descarga = document.getElementById("download");
  descarga.href = url;

  console.log(url);
  var pr = url.indexOf("/");
  console.log(pr);
  console.log(url.length);
  var pr = url.substring(pr+1, url.length);
  console.log(pr);
  descarga.download = pr;

  document.getElementById('tituloReproduciendo').innerHTML = titulo;
  document.getElementById('descripcionReproduciendo').innerHTML = artista + "-" + ciudad + "-" + year;
  colorAntiguo = document.getElementById(titulo+id);
  document.getElementById('imagenReproductor').src = rutaImg;

}

var videos = document.getElementsByClassName("backVideo")
for (var i = videos.length - 1; i >= 0; i--) {
  videos[i].addEventListener("mouseover", function () {
    if(this.id !== "videoPrincipal"){


    this.id="videoA";
    this.className = "backVideo col-md-12";
    }
  });
  videos[i].addEventListener("mouseout", function () {
    if(this.id !== "videoPrincipal"){
      this.className = "backVideo col-md-4";
    }
  })

}



