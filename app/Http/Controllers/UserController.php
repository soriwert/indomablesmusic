<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    function index()
    {
        if (request()->has('empty')){
            $users = [];
        } else {

            $users = [
                'patri',
                'juan',
                'manolo'
            ];
        }
        return view('users', [
            'users' => $users,
            'title' => 'lista de usuarios'
        ]);

    }

    function show($id)
    {
        return "Mostrando detalle:  5";
    }

    function create()
    {
        return 'Crear nuevo user';
    }
}
