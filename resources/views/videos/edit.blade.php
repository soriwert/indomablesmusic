@extends('layouts.app')
@section('content')
<h1>Edición de grupo</h1>


<form method="post" action="/videos/{{ $video->id }}">
{{ csrf_field() }}
<input type="hidden" name="_method" value="put">
<div class="form-group">
    <label>Id</label>
    <input type="text" name="id" class="form-control" value="{{ $video->id }}" disabled="true">
</div>


<script>
function controlURL() {
    var url = document.getElementById("url");
    if(url.value.indexOf("https://www.youtube.com/embed/") != -1){
        url.style.color="green";
        var indice = url.value.indexOf("d/");
        var final = url.value.length;
        var id = url.value.substr(indice+2, final).length;
        if(id<11){
            alert("el id de la url no es correcto, debe tener 11 caracteres o más.");
            url.value = "https://www.youtube.com/embed/";
            url.style.color = "blue"
        }
    }else if(url.value.indexOf("https://www.youtube.com/watch?v=") != -1){
        var indice = url.value.indexOf("=");
        var final = url.value.length;
        var id = url.value.substr(indice+1, final);
        if(id.length<10){
            alert("el id de la url no es correcto, debe tener 11 caracteres o más.");
            url.value = "https://www.youtube.com/embed/";
            url.style.color = "blue;"
        }else{
            url.value = "https://www.youtube.com/embed/"+id;
            controlURL();
        }
    }else{
        alert("Recuerda que el campo debe de ser embed y no watch")
        url.value = "Error formato incorrecto";
        url.style.color="red";
    }
}
</script>

     <div class="form-group">
         <label>Url</label>
         <input id="url" type="text" name="url" class="form-control" value="{{ $video->url }}" onchange="controlURL()">
    <label>Titulo</label>
    <input type="text" name="titulo" class="form-control" value="{{ $video->titulo }}">
    <label>Artista</label>
    <input type="text" name="artista" class="form-control" value="{{ $video->artista }}">
    <label>ciudad</label>
    <input type="text" name="ciudad" class="form-control" value="{{ $video->ciudad }}">
    <label>year</label>
    <input type="text" name="year" class="form-control" value="{{ $video->year }}">
</div>

    <div class="alert-danger">
        {{$errors->first('video')}}
    </div>

    <div class="form-group">
        <input type="submit" name="" value="Editar" class="form-control">
    </div>
</form>

@endsection
