@extends('layouts.app')
@section('content')
<h1>Edición de grupo</h1>


<form method="post" action="/canciones/{{ $cancion->id }}">
{{ csrf_field() }}
<input type="hidden" name="_method" value="put">
<div class="form-group">
    <label>Id</label>
    <input type="text" name="id" class="form-control" value="{{ $cancion->id }}" disabled="true">
</div>
</script>

     <div class="form-group">
         <label>Url</label>
         <input id="url" type="text" name="url" class="form-control" value="{{ $cancion->url }}">
    <label>Titulo</label>
    <input type="text" name="titulo" class="form-control" value="{{ $cancion->titulo }}">
    <label>Artista</label>
    <input type="text" name="artista" class="form-control" value="{{ $cancion->artista }}">
    <label>ciudad</label>
    <input type="text" name="ciudad" class="form-control" value="{{ $cancion->ciudad }}">
    <label>year</label>
    <input type="text" name="year" class="form-control" value="{{ $cancion->year }}">
</div>

    <div class="alert-danger">
        {{$errors->first('cancion')}}
    </div>

    <div class="form-group">
        <input type="submit" name="" value="Editar" class="form-control">
    </div>
</form>

@endsection
