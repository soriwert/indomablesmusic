<?php

use Illuminate\Database\Seeder;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //['id', 'local', 'youtube'];

        DB::table('videos')
        ->insert([
            'id' => 1,
            'url' => 'https://www.youtube.com/embed/Th2GbeNDBxY',
            'titulo' => 'Pasan las horas',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 2,
            'url' => 'https://www.youtube.com/embed/HKQAit-7HzU',
            'titulo' => 'El amor murio',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 3,
            'url' => 'https://www.youtube.com/embed/pxkrW9BnhBQ',
            'titulo' => 'Niña Especial',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 4,
            'url' => 'https://www.youtube.com/embed/bEJNjHaYCRQ',
            'titulo' => 'Una pregunta',
            'artista' => 'Mc_Soriano',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 5,
            'url' => 'https://www.youtube.com/embed/7CI-Tf26wF4',
            'titulo' => 'Destino',
            'artista' => 'Mc_Soriano',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 6,
            'url' => 'https://www.youtube.com/embed/U0o1cTZFeKo',
            'titulo' => 'Lose Yourself (Cover)',
            'artista' => 'Mc_Soriano',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
        DB::table('videos')
        ->insert([
            'id' => 7,
            'url' => 'https://www.youtube.com/embed/v8weGnQAgas',
            'titulo' => 'Problemas y decisiones',
            'artista' => 'Mc_Soriano',
            'ciudad' => 'Zaragoza',
            'year' => '2018'
        ]);
    }
}
