<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'comentario'];

    function user()
    {
        return $this->belongsTo('App\User');
    }
}
