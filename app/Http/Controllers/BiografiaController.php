<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiografiaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', Biografia::class);
        return view('biografia');
    }
}
