<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersModuleTest extends TestCase
{
    /** @test */
    function comprobarUrlUsers()
    {
        $this->get('/users')
        ->assertStatus(200)
        ->assertSee('usuarios')
            ->assertSee('manolo')
            ->assertSee('patri')
            ->assertSee('juan');
    }

    /** @test */
    function comprobarUrlDetalles()
    {
        $this->get('users/5')
        ->assertStatus(200)
        ->assertSee("Mostrando detalle:  5");
    }

    /** @test */
    function comprobarUrlNuevoUsuario()
    {
        $this->get('users/new')
        ->assertStatus(200)
        ->assertSee("Crear nuevo user");
    }

    /** @test */
    function mostrarListadoUsuarios()
    {
        $this->get('/users?empty')
            ->assertSee('No hay usuarios Registrados actualmente');
    }


}
