<?php

use Illuminate\Database\Seeder;

class CancionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('canciones')
        ->insert([
            'url' => 'archivos/El_Amor_Murio.mp3',
            'titulo' => 'El amor murio',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018',
            'urlImg' => '/images/El amor murio.jpg'
        ]);
        DB::table('canciones')
        ->insert([
            'url' => 'archivos/Niña_Especial.mp3',
            'titulo' => 'Niña Especial',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018',
            'urlImg' => '/images/Niña Especial.jpg'
        ]);
        DB::table('canciones')
        ->insert([
            'url' => 'archivos/Pasan_Las_Horas.mp3',
            'titulo' => 'Pasan las horas',
            'artista' => 'Sori x El Makial',
            'ciudad' => 'Zaragoza',
            'year' => '2018',
            'urlImg' => '/images/Pasan las horas.jpg'
        ]);
    }
}
