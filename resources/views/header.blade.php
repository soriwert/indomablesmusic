<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Indomables Music</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
    <!--Header-->
    <div class="container-fluid">
        <div class="iconClassLog" id="iconLog">
            <div class="centrado">
                    <i class="fa fa-2x fa-user-circle-o" style="font-size: 3em"></i>
                </div>

        </div>
        <div class="iconClassLog">
            @guest
            <div id="login" class="login">
                <form class="form-inline" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail') }}</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="checkbox" for="remember">
                            <label>
                                <input type="checkbox"
                                name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                    <a id="register" class="btn btn-warning" href="{{ route('register') }}">Register</a>
                    <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                </form>
            </div>

            @else
            <div class="iconClassLog" id="iconLog">
                <div class="centrado">
                    <i class="fa fa-2x fa-user-circle-o" style="font-size: 3em"></i>
                </div>
            <div id="userRegistered">

                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu bg-warning" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>

        </div>

        @endguest
    </div>
    <div class="row justify-content-center">

    </div>




    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="5000">
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/Background3.jpg" alt="Los Angeles" class="img-fluid img100">
                <div class="carousel-caption"></div>
            </div>
            <div class="carousel-item">
                <img src="images/Background2.jpg" alt="Chicago" class="img-fluid img100">
                <div class="carousel-caption"></div>
            </div>
            <div class="carousel-item">
                <img src="images/Background.png" alt="New York" class="img-fluid img100">
                <div class="carousel-caption"></div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
    <div class="row">
        <nav id="nav" class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav col-md-12">
                <li class="nav-item col-2">
                    <a href="/home" class="nav-link btn-floating btn-sm btn-li mx-1">
                        <i class="fa fa-home menu"></i>
                        <span class="fontFamily">&nbsp;Inicio</span>
                    </a>
                </li>

                <li class="nav-item dropdown col-2">
                    <a class="nav-link btn-floating btn-sm btn-li mx-1" href="" id="Biografia" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa fa-leanpub menu"></i>
                        <span class="fontFamily">&nbsp;Biografía</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item fa" href="/biografia/fauder">Fauder</a>
                        <a class="dropdown-item fa" href="/biografia/sori">Sori</a>
                        <a class="dropdown-item fa" href="/biografia">Indomables</a>
                    </div>
                </li>
                <li class="nav-item col-2">
                    <a href="/canciones" class="nav-link btn-floating btn-sm btn-li mx-1">
                        <i class="fa fa-play menu"></i>
                        <span class="fontFamily">&nbsp;Discografía</span>
                    </a>
                </li>
                <li class="nav-item col-2">
                    <a href="/videos" class="nav-link btn-floating btn-sm btn-li mx-1">
                        <i class="fa fa-youtube-play menu"></i>
                        <span class="fontFamily">&nbsp;Videos</span>
                    </a>
                </li>
                <li class="nav-item col-2">
                    <a href="/eventos" class="nav-link btn-floating btn-sm btn-li mx-1">
                        <i class="fa fa-microphone menu"></i>
                        <span class="fontFamily">&nbsp;Eventos</span>
                    </a>
                </li>
                <li class="nav-item col-2">
                    <a href="/merchandising" class="nav-link btn-floating btn-sm btn-li mx-1">
                        <i class="fa fa-shopping-cart menu"></i>
                        <span class="fontFamily">&nbsp;Merchandising</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
