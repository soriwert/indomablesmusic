<div id="sidebarTop" class="col-md-3">

    <div class="row">
        <h4 style="visibility: hidden">.</h4>
    </div>

    <div class="row" id="divCard">
        <div class="card text-md-center bg-info mb-3 col-12">
            <div id="card" class="card-header mb-3" ">
                <h3>Top 3</h3>
                <h5>Descargas</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">
                    <a href="https://www.youtube.com/channel/UCV-Ci_idBzi6Kb1481owyyw" class="white">
                        <i id="btnYoutube" class="fa fa-youtube-play"></i>Indomables Music
                    </a>
                </h5>
            </div>


            <div class="card-text">

                <div class="row">
                    <div class="col-md-12">
                        <a href="https://www.youtube.com/watch?v=Th2GbeNDBxY" class="white">1. Pasan las horas</a>
                    </div>
                    <div class="col-md-12">
                        <a href="https://www.youtube.com/watch?v=HKQAit-7HzU" class="white">2. El amor murio</a>
                    </div>
                    <div class="col-md-12">
                        <a href="https://www.youtube.com/watch?v=pxkrW9BnhBQ" class="white">3. Niña especial</a>
                    </div>
                </div>

            </div>
        </div>


    </div>


    <div class="row" id="divInsta">
        <div class="row">
            <div class="col-md-5">
                <div class="media p-3">
                    <img class="mr-3 mt-3 rounded-circle" src="https://instagram.fmad8-1.fna.fbcdn.net/vp/a23507f8db0000bc80f4c8790b23d53c/5B80F1C6/t51.2885-19/s150x150/26871179_246338462572227_561176277956952064_n.jpg"
                    alt="Foto de Instagram" style="width: 100%;">
                </div>
            </div>

            <div class="col-md-6">
                <div class="media p-3">
                <img src="images/logoInstagram.png" alt="Icono de Instagram" class="mr-3 mt-3" style="width: 100%;">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="media">
                <img src="images/foto1.PNG" alt="Foto1" class="mt-3 mr-3" style="width: 100%;">
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                <img src="images/foto2.PNG" alt="Foto2" class="mt-3 mr-3" style="width: 100%;">
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                <img src="images/foto3.png" alt="Foto3" class="mt-3 mr-3" style="width: 100%;">
                </div>
            </div>
        </div>









        <div class="row">
            <div class="col-4">
                <div class="media">
                  <img src="https://scontent.fmad3-6.fna.fbcdn.net/v/t1.0-1/p200x200/17626300_1878315089091948_3625729997601583279_n.jpg?_nc_cat=0&oh=5d8266602945f25b17da42644cf9d0d1&oe=5BB68C26" alt="Foto de Facebook" class="mr-3 mt-3 rounded-circle" style="width:100%;">
              </div>
          </div>


          <div class="col-6">
            <div class="media p-3">
              <img src="images/facebook.png" alt="Foto de Facebook" class="mr-3 mt-3" style="width:100%;">
          </div>
      </div>

      <div class="col-2"></div>
  </div>
  <div class="row marginBottom">
    <div class="col-4">
        <div class="media">
            <img src="https://scontent.fmad3-6.fna.fbcdn.net/v/t1.0-0/c40.0.200.200/p200x200/26907869_2012170522373070_9125163642815608158_n.jpg?_nc_cat=0&oh=80c6ed5d28da59125195d731e63a8c1a&oe=5BB95C2A" alt="foto1" class="mr-3 mt-3" style="width:100%;">
        </div>
    </div>
    <div class="col-4">
        <div class="media">
            <img src="https://scontent.fmad3-6.fna.fbcdn.net/v/t1.0-0/p200x200/26814839_2009477379309051_619420489823135932_n.jpg?_nc_cat=0&oh=aac8099cf8c9383c903e4e2fd076a5a5&oe=5B899BFF" alt="foto2" class="mr-3 mt-3" style="width:100%;">
        </div>
    </div>
    <div class="col-4">
        <div class="media">
            <img src="https://scontent.fmad3-6.fna.fbcdn.net/v/t1.0-0/p200x200/23131864_1977106635879459_1035284318150884739_n.jpg?_nc_cat=0&oh=d059a61ed800dea8cd4ed2397dd9d38c&oe=5B7F015E" alt="foto3" class="mr-3 mt-3" style="width:100%;">
        </div>
    </div>
</div>
</div>
</div>

