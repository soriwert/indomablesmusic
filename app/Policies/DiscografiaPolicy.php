<?php

namespace App\Policies;

use App\User;
use App\Discografia;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiscografiaPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if($user->roleId == 1){
            return true;
        }
    }

    /**
     * Determine whether the user can view the cancion.
     *
     * @param  \App\User  $user
     * @param  \App\Discografia  $cancion
     * @return mixed
     */
    public function view(User $user, Discografia $cancion)
    {
        return true;
    }

    /**
     * Determine whether the user can create canciones.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->roleId == 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can update the cancion.
     *
     * @param  \App\User  $user
     * @param  \App\Discografia  $cancion
     * @return mixed
     */
    public function update(User $user, Discografia $cancion)
    {
        //dd($user);
        if($user->roleId == 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can delete the cancion.
     *
     * @param  \App\User  $user
     * @param  \App\Discografia  $cancion
     * @return mixed
     */
    public function delete(User $user, Discografia $cancion)
    {
        if($user->roleId == 1){
            return true;
        }else{
            return false;
        }
    }
}
