<?php

namespace App\Http\Controllers;

use App\Cancion;
use App\User;
use Illuminate\Http\Request;
use App\Mensajes;

class CancionController extends Controller
{

    public function index()
    {
        //$this->authorize('view', cancion::class);
        $mensajes = Mensajes::with('user')->get();
        $canciones = Cancion::paginate(10);
        $canciones = Cancion::all();
        return view('canciones.index', ['canciones' => $canciones, 'mensajes'=>$mensajes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Cancion::class);
        return view('canciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update', Cancion::class);

        //validacion trait
        $this->validate($request, [
            'url' => 'required|max:255',
            'titulo' => 'required|max:50',
            'artista' => 'required|max:50',
            'ciudad' => 'required',
            'year' => 'required',
        ]);
        $cancion = new Cancion($request->all());
        $cancion->save();
        return redirect('/canciones');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Cancion::class);
        $cancion = Cancion::where('id', $id)->first();
        return view('canciones.edit', ['cancion' => $cancion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Cancion::class);
        $this->validate($request, [
            'url' => 'required|max:255',
            'titulo' => 'required|max:50',
            'artista' => 'required|max:50',
            'ciudad' => 'required',
            'year' => 'required',
        ]);
        $cancion = Cancion::find($id);
        $cancion->fill($request->all());
        $cancion->save();
        return redirect('/canciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Cancion::class);
        Cancion::destroy($id);
        return redirect("/canciones");
    }

    public function agregarComentario(Request $request)
    {
        $mensaje = new Mensajes;
        $mensaje->comentario = $request->input('mensaje');
        if($request->input('user_id')){
            $mensaje->user_id = $request->input('user_id');
        } else {
            $mensaje->user_id = null;
        }
        $mensaje->save();
        return redirect('/canciones');
    }
}
