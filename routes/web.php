<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', 'HomeController@index');
Route::resource('/canciones', 'CancionController');
Route::get('/biografia', 'BiografiaController@index');
Route::resource('videos', 'VideoController');
Route::get('/eventos', 'HomeController@eventos');
Route::get('/merchandising', 'HomeController@merchandising');
Route::get('/login, HomeController@login');
Route::get('/logout', 'HomeController@logout');
Route::post('/canciones/comentario/{id}', 'CancionController@agregarComentario');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');

