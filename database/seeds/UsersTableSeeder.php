<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'name' => 'alejandro',
            'email' => 'alejandro@gmail.com',
            'password' => bcrypt('pass'),
            'roleId' => 1
        ]);
        DB::table('users')
        ->insert([
            'name' => 'patricio',
            'email' => 'patricio@gmail.com',
            'password' => bcrypt('pass'),
            'roleId' => 2
        ]);
        DB::table('users')
        ->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'roleId' => 1
        ]);
        DB::table('users')
        ->insert([
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('secret'),
            'roleId' => 2
        ]);

        factory(App\User::class, 10)->create();

        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);

    }
}
