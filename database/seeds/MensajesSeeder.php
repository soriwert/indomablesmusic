<?php

use Illuminate\Database\Seeder;

class MensajesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('mensajes')
        ->insert([
            'user_id' => 1,
            'comentario' => 'asdfasdf'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 1,
            'comentario' => 'fasdfasdf'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 1,
            'comentario' => 'asdfwerewr'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 1,
            'comentario' => '123412341234'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 2,
            'comentario' => 'd fg gdfsg sdfg'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 2,
            'comentario' => '12 '
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 2,
            'comentario' => 'sa1df321sad3f21as32df1'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 2,
            'comentario' => 'd87fas8df7a*s/df7'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 3,
            'comentario' => 'as9df8489sd4f65a4sdf687sd86fasdf'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 3,
            'comentario' => 'sa5df465as4df65462154 5346 5465446 43644 '
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 3,
            'comentario' => 'as5df45asd4fa6sd8f4533 854 8634 635 43684 6384 63'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 3,
            'comentario' => 'asdfasdf'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 4,
            'comentario' => 'asdfasdf12312321312'
        ]);
        DB::table('mensajes')
        ->insert([
            'user_id' => 1,
            'comentario' => 'asdfasdf23432'
        ]);

    }
}
