



        <footer class="page-footer font-small stylish-color-dark pt-4 mt-4">

            <!--Footer Links-->
            <div class="container text-center text-md-left">
                <div class="row">
                    <!--First column-->
                    <div class="col-md-4">
                        <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">
                            <i class="fa fa-registered"></i>
                            DERECHOS RESERVADOS
                        </h5>
                        <p>
                            <a href="#">Política de Privacidad</a>
                            <br>
                            <a href="#">Política de cookies</a>
                            <br>
                            <a href="#">© 2018 Indomables Music Group</a>
                            <br>
                        </p>
                    </div>
                    <!--/.First column-->

                    <hr class="clearfix w-100 d-md-none">

                    <!--Second column-->
                    <div class="col-md-2 mx-auto">
                        <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Links</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Second column-->

                    <hr class="clearfix w-100 d-md-none">

                    <!--Third column-->
                    <div class="col-md-2 mx-auto">
                        <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Links</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Third column-->

                    <hr class="clearfix w-100 d-md-none">

                    <!--Fourth column-->
                    <div class="col-md-2 mx-auto">
                        <h5 class="text-uppercase mb-4 mt-3 font-weight-bold">Links</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Fourth column-->
                </div>
            </div>
            <!--/.Footer Links-->
            <hr>
            <!--Social buttons-->
            <div class="text-center">
                <ul class="list-unstyled list-inline">
                    <li class="list-inline-item">
                        <a href="#" class="btn-floating btn-sm btn-fb mx-1">
                            <i class="fa fa-2x fa-facebook-official"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="btn-floating btn-sm btn-tw mx-1">
                            <i class="fa fa-2x fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="btn-floating btn-sm btn-gplus mx-1">
                            <i class="fa fa-2x fa-google-plus"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="btn-floating btn-sm btn-li mx-1">
                            <i class="fa fa-2x fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!--/.Social buttons-->

        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script type="text/javascript" src=/js/js.js></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
</body>

</html>
