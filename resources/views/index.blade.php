@include('header')
<div id="contenido">
    <div class="row">
        <div class="col-md-2">
            <img src="images/Background.png" width="100%">
        </div>
        <div class="col-md-7">

            <h1>Bienvenidos a la casa de los Indomables Music</h1>
            <p>
                Somos un duo que comenzo en el año 2015 a escribir, muchas experiencias y amistades
                en el transcurro del tiempo. Las experiencias que hemos tenido hasta el momento han
                sido satisfactorias, hemos conocido mucha gente, hemos viajado a lugares para grabar
                y sobre todo hemos reido mucho.
            </p>
            <p>
                Lo tomamos como un Hobby ya que no creemos que lleguemos a lo más alto de la música
                puesto que esta prácticamente en la actualidad es dinero. Aunque la suerte nunca sabe
                uno cuando llega.
            </p>
            <p>
                Si estás aqui es porque... te gusta nuestra música... has sido curioso investigando o
                simplemente querías dar un paseo por nuestra web de nuevo. Solo por eso te damos la bienvenida
                y agradeceríamos que para poder descargar nuestro material te subscribas a nuestra página con un registro sencillo.
            </p>
        </div>
    @include('sidebarTop')
    </div>
</div>
@include('footer')
