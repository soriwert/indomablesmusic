@include('header')
<div id="marginRow" class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="backcancion col-md-12">
                <div id="reproductor" class="card">
                    @foreach ($canciones as $cancion)
                    @if($cancion->id == 1)
                    @can ('create', $cancion)
                    <a class="btn btn-secondary" href="/canciones/create" name="Crear">Crear nueva cancion</a>
                    @endcan

                    <div class="row fondoNegro">
                        <div class="col-sm-12 col-md-3"></div>
                        <div class="col-sm-12 col-md-6">
                            <img id="imagenReproductor" class="card-img-top img-thumbnail" src="images/foto1.PNG" alt="Card image cap">
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="col-md-12">
                                <h3 class="centrarTexto listaReproduccion">Lista de reproducción</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-12 scroll">
                                    @foreach ($canciones as $cancion)

                                    <ul class="list-group">

                                        <li id="{{$cancion->titulo}}{{$cancion->id}}" class="list-group-item transparent btn btn-outline-primary"
                                            onmouseover="agrandar('{{$cancion->titulo}}{{$cancion->id}}')"
                                            onmouseout="encoger('{{$cancion->titulo}}{{$cancion->id}}')"
                                            onclick="cambiaCancion('{{$cancion->url}}','{{$cancion->id}}','{{$cancion->titulo}}', '{{$cancion->artista}}', '{{$cancion->ciudad}}', '{{$cancion->year}}', '{{$cancion->urlImg}}')">
                                            {{$cancion->id}}. {{$cancion->titulo}}
                                        </li>
                                    </ul>
                                    @endforeach
                                </div>
                            </div>

                            <script>

                                function agrandar(cancionId) {
                                    var liSobrepasado = document.getElementById(cancionId);
                                    liSobrepasado.style.fontSize = "1em";
                                    liSobrepasado.style.backgroundColor = "gray";
                                }
                                function encoger(cancionId) {
                                    var liAbandonado = document.getElementById(cancionId);
                                    liAbandonado.style.fontSize = "0.7em";
                                    liAbandonado.style.backgroundColor = "transparent";
                                }

                            </script>

                        </div>
                    </div>

                    <div class="row btn btn-outline-info background">
                        <div class="col-12 col-sm-12 col-md-12">
                            <h2 id="tituloReproduciendo" class="card-title centrarTexto"></h2>
                            <h5 id="descripcionReproduciendo" class="card-text centrarTexto"></h5>
                        </div>
                    </div>
                    <div class="card-body fondoNegro">

                        <div class="row">
                            <div class="col-sm-1 col-md-3"></div>
                            <div class="col-12 col-sm-1 col-md-1">
                                <audio id="player{{$cancion->id}}" src="{{$cancion->url}}"></audio>
                                <button id="play" class="btn btn-success"><i class="fa fa-play"></i></button>
                            </div>
                            <div class="col-12 col-sm-1 col-md-1">
                                <button id="pause" class="btn btn-default"><i class="fa fa-pause"></i></button>
                            </div>
                            <div class="col-12 col-sm-1 col-md-1">
                                <button id="stop" class="btn btn-danger"><i class="fa fa-stop"></i></button>
                            </div>
                            <div class="col-12 col-sm-1 col-md-1">
                                <a id="download" href="{{$cancion->url}}" download="{{$cancion->titulo}}" class="btn btn-primary"><i class="fa fa-download"></i></a>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <button class="btn btn-info">
                                        <form>
                                            <input id="volume" type="range" min="0" max="1" step="0.01" value="1"
                                            onchange="document.getElementById('player{{$cancion->id}}').volume = document.getElementById('volume').value;"/>
                                        </form>
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        <div class="row">
                            @foreach($mensajes as $mensaje)
                            @if ($mensaje->user)
                            <div class="col-md-12">
                                <div class="card text-white bg-dark mb-3">
                                <div class="card-header">{{$mensaje->user->name}} ha comentado:</div>
                                  <div class="card-body cardComentario">
                                    <p class="card-text"><span>{!!$mensaje->comentario!!}</span></p>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-md-12">
                            <div class="card text-white bg-dark mb-3">
                              <div class="card-header">Anonimo ha comentado</div>
                              <div class="card-body cardComentario">
                                <p class="card-text"><span>{!!$mensaje->comentario!!}</span></p>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div id="textArea" class="col-md-8">
                    <form method="post" action="/canciones/comentario/{{ Auth::user() ? Auth::user()->id : 0}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{Auth::user() ? Auth::user()->id : 0}}">
                        <div class="form-group">
                            <label for="comment">Escribe aquí tu comentario:</label>
                            <textarea name="mensaje" class="form-control" rows="5" id="comment">


                            </textarea>
                        </div>

                        <div class="form-group">
                          <input type="submit" name="Enviar" value="Enviar" class="form-control">
                      </div>
                  </form>
              </div>

              <div class="col-md-2"></div>
          </div>

      </div>
  </div>
</div><!--colmd12-->
</div>
@include('sidebarTop')
</div>
@include('footer')



