@include('header')


<div id="marginRow" class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
                <img id="fauder" src="images/fauderFollow.png" alt="Imagen de Fauder" class="img100">
            </div>
        </div>
    </div>
    @foreach ($videos as $video)

    @if($video->id == 1)

    <div class="col-md-6">

        <div class="row">
            <div class="col-md-9">

                <h3>{{$video->titulo}}</h3>
            </div>
            <div class="col-md-3">
                @can ('create', $video)
                <a class="btn btn-secondary" href="/videos/create" name="Crear">Crear nuevo video</a>
                @endcan
            </div>
        </div>
        <div class="row">
            <div  id="videoPrincipal" class="backVideo col-md-12">
                <div class="embed-responsive embed-responsive-21by9">
                    <iframe class="videos" width="560" height="315" src="{{$video->url}}" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        @else


        <div class="row">


        <div class="col-md-4">
                <tr>
                    <td>
                        <form method="post" action="/videos/{{ $video->id }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete">

                            @can ('view', $video)
                            <a class="btn btn-secondary" href="/videos/{{ $video->id }}">Ver</a>
                            @endcan


                            @can ('update', $video)
                            <a class="btn btn-secondary" href="/videos/{{ $video->id }}/edit">Editar</a>
                            @endcan

                            @can ('delete', $video)
                            <input class="btn btn-secondary" type="submit" name="Borrar" value="Borrar">
                            @endcan

                        </form>
                    </td>
                </tr>
            </div>
            <div class="backVideo col-md-4">
                <div class="embed-responsive embed-responsive-21by9">
                    <iframe class="videos" width="560" height="315" src="{{$video->url}}" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-4 leftVideo">
                <h4>{{$video->titulo}}</h4>
                <h5>{{$video->artista}}</h5>
                <p>{{$video->ciudad}}</p>
                <p>{{$video->year}}</p>
            </div>

        </div>
        @endif
        @endforeach

    </div>

    @include('sidebarTop')
</div>
@include('footer')

