@extends('layouts.app')
@section('content')
<h1>Alta de video</h1>

<form method="post" action="/canciones">
    {{ csrf_field() }}

    <div class="form-group">
    <label>Url</label>
    <input type="text" name="url" class="form-control" value="{{ old('url') }}">
    <label>Titulo</label>
    <input type="text" name="titulo" class="form-control" value="{{ old('titulo') }}">
    <label>Artista</label>
    <input type="text" name="artista" class="form-control" value="{{ old('artista') }}">
    <label>ciudad</label>
    <input type="text" name="ciudad" class="form-control" value="{{ old('ciudad') }}">
    <label>year</label>
    <input type="text" name="year" class="form-control" value="{{ old('year') }}">

    </div>

    <div class="alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>

     <div class="form-group">
    <input type="submit" name="Enviar" value="Enviar" class="form-control">
    </div>
</form>
